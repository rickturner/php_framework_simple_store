<?php


namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\core\components\ItemDisplay;




class ItemController extends Controller
{
    public function item(Request $request){

      
        if ($request->isPost()){
            $data  = $request->getBody();
            $className =  ucfirst($data['type']);
            $namespace = "\\app\\models\\";
            $className = "$namespace" . $className;
            $productModel = new $className;

            $productModel->loadData($request->getBody());

            if ($productModel->validate() && $productModel->register()){
                Application::$app->response->redirect("/add-product");
            }else{
            }
            return $this->render('add_item', [
                'model' => $productModel
            ]);
        }

        return $this->render('add_item', [
            'model' => ""
        ]);
    }

    public function home(Request $request){


        $requestBody = $request->getBody();
        ItemDisplay::massDelete($requestBody);
        Application::$app->response->redirect("/");

      
    }

}
