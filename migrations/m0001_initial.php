<?php

use app\core\Application;

class m0001_initial
{
    public function up()
    {
        $db = Application::$app->db;
        $SQL = "CREATE TABLE items (
                id INT AUTO_INCREMENT PRIMARY KEY,
                sku VARCHAR(10) NOT NULL,
                name VARCHAR(255) NOT NULL,
                price FLOAT NOT NULL,
                type VARCHAR(255) NOT NULL,
                size VARCHAR(255),
                weight VARCHAR(255),
                height VARCHAR(255),
                length VARCHAR(255),
                width VARCHAR(255)
                ) 
                ENGINE=INNODB;";

        $db->pdo->exec($SQL);
    }

    public function down()
    {
        $db = Application::$app->db;
        $SQL = "DROP TABLE items";

        $db->pdo->exec($SQL);
    }
}
