<?php

use app\core\Application;
use app\core\components\ItemDisplay;
use app\core\Model;



$itemsObj = new ItemDisplay();

$skus = $itemsObj->getSkus();



?>

<style>
    <?php include Application::$app::$ROOT_DIR . '/views/static/css/form.css';  ?>
</style>

<div class="container" id="app">

    <form action="" method="POST" id="product_form">
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label id="sku-label" for="sku" class="form-label">SKU</label>
                    <input id="sku" @input="validateSKU" type="text" name="sku" aria-describedby="emailHelp" value="" class="form-control">
                    <div v-if="skuNotUniqueError" class="alert alert-danger  fade show" role="alert">
                        <strong>SKU error!</strong> This SKU has already been registered
                    </div>
                    <div v-if="skuLengthError" class="alert alert-danger  fade show" role="alert">
                        <strong>SKU error!</strong> The SKU should be 10 characters long
                    </div>
                </div>
            </div>
            <div class="col">

                <div class="mb-3">
                    <label id="name-label" for="sku" class="form-label">Name</label>
                    <input id="name" @input="validateName" type="text" name="name" aria-describedby="emailHelp" value="" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="productType" class="form-label">Type</label>
                    <select v-model="type" @change="getChoice" class="form-select" aria-label="Default select example" name="type" id="productType">
                        <option value="book" selected>Book</option>
                        <option value="furniture">Furniture</option>
                        <option value="dvd">DVD</option>
                    </select>
                </div>
            </div>
            <div class="col">

                <div class="mb-3">
                    <label id="price-label" for="price" class="form-label">Price</label>
                    <input id="price" @input="validatePrice" type="text" name="price" aria-describedby="emailHelp" value="" class="form-control">
                    <div v-if="priceValueError" class="alert alert-danger  fade show" role="alert">
                    <strong>Price Error!</strong>Please Make sure that price is a numeric value
                </div>
                </div>
            </div>
        </div>


        <!-- Book -->
        <div v-if='type === "book"' class="row">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Weight (Kg)</label>
                <input @input='validateAttribute' type="text" id="weight" name="weight" class="form-control" id="exampleFormControlInput1" placeholder="200">
                <div v-if="attributeErrorNonNumeric" class="alert alert-danger  fade show" role="alert">
                    <strong>Book Weight value error! </strong>Please Make sure that weight is a numeric value
                </div>

            </div>
        </div>

        <!-- Furniture -->
        <div v-if='type === "furniture"' class="row furniture-inputs">
            <div class="col">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Height (Cm)</label>
                    <input @input='validateHeight' type="text" id="height" name="height" class="form-control" id="exampleFormControlInput1" placeholder="200">
                </div>
            </div>

            <div class="col">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Width (Cm)</label>
                    <input @input='validateWidth' type="text" id="width" name="width" class="form-control" id="exampleFormControlInput1" placeholder="200">
                </div>
            </div>


            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Length (Cm)</label>
                <input @input='validateLength' type="text" id="length" name="length" class="form-control" id="exampleFormControlInput1" placeholder="200">

            </div>

        </div>

        <!-- Dvd -->
        <div v-if='type === "dvd"' class="row">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Size (MB)</label>
                <input @input='validateAttribute' type="text" id="size" name="size" class="form-control" id="exampleFormControlInput1" placeholder="200">
                <div v-if="attributeErrorNonNumeric" class="alert alert-danger  fade show" role="alert">
                    <strong>DVD Size value error! </strong>Please Make sure that size is a numeric value
                </div>
            </div>
        </div>

        <div class="row">
            <div class="d-flex form-btns">

                <button type="submit" name="submit" class="btn btn-submit">Save</button>
                <a href="/" class="btn btn-danger" id="reset">Cancel</a>

            </div>
        </div>
    </form>





    <div class='card' style='width: 20rem'>
        <div class='card-body'>
            <h5 class='card-title text-center'>{{name}}</h5>

            <div v-if='price' class='card-items'>
                <p class='card-items-label'>Price</p>
                <p class='card-text card-price'>{{price}}$</p>
            </div>
            <div class='card-items'>
                <p v-if='type == "book" && attribute' class='card-items-label'>Weight</p>
                <p v-if='type == "dvd" && attribute' class='card-items-label'>Size</p>
                <p v-if='type == "furniture" && attribute' class='card-items-label'>Dimensions</p>

                <p class='card-text card-attribute'>{{attribute}}</p>
            </div>
            <div class='card-bottom'>
                <p class='card-text card-type badge rounded-pill'>{{type}}</p>
                <p class='card-text card-sku  badge rounded-pill'>{{sku}}</p>

            </div>
        </div>
    </div>

</div>



<script>
    <?php include Application::$app::$ROOT_DIR . '/views/static/js/validate_form.js';  ?>
</script>
<script type="text/javascript">
    let skuList = <?php echo json_encode($skus); ?>
</script>
<script>
    <?php include Application::$app::$ROOT_DIR . '/views/static/js/app.js';  ?>
</script>