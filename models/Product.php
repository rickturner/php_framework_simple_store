<?php
namespace app\models;
namespace app\models;

use app\core\DbModel;

abstract class Product extends DbModel
{

    protected string $sku = "";
    protected string $name = "";
    protected string $price = "";

    // SETTERS
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    #GETTERS
    public function getSku()
    {
        if ($this->sku) {
            return $this->sku;
        }
    }

    public function getName()
    {
        if ($this->name) {
            return $this->name;
        }
    }

    public function getPrice()
    {
        if ($this->price) {
            return $this->price;
        }
    }

    

    public function tablename(): string
    {
        return "items";
    }

    public function register()
    {
        return $this->save();
    }

    public function labels(): array
    {
        return [
            "sku" => "SKU",
            "name" => "Name",
            "price" => "Price",
            "type" => "Type",
            "attribute" => "Attribute"

        ];
    }

    abstract function setAttribute($attribute);
    abstract function getAttribute();
    abstract function getType();
    abstract function getLabel();
    abstract function populateClassAttributes(array $item);
    public function getHtmlContent(){
    
    $ItemHtml = "<div class='card card-hidden-border' style='width: 20rem'>
    <input type='checkbox' name=%s class='delete-checkbox'></input>
        <div class='card-body'>
            <h5 class='card-title'>%s</h5>
          
            <div class='card-items'>
                <p class='card-items-label'>Price</p>
                <p class='card-text card-price'>%s$</p>
            </div>
            <div class='card-items'>
                <p class='card-items-label'>%s</p>
                <p class='card-text card-attribute %s'> %s</p>
            </div>
            <div class='card-bottom'>
            <p class='card-text card-type card-type-%s badge rounded-pill'>%s</p>
            <p class='card-text card-sku  badge rounded-pill'>%s</p>
            
            </div>
            </div>
    </div> ";
        
             return sprintf(
                
                $ItemHtml,
                $this->getSku(),
                $this->getName(),
                $this->getPrice(),
                $this->getType(),
                $this->getLabel(),
                $this->getAttribute(),
                $this->getType(),
                $this->getType(),
                $this->getSku(),
            );
        }
    }