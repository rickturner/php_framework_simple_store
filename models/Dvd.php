<?php

namespace app\models;
use app\models\Product;


class Dvd extends Product{
    protected string $type = "dvd";
    protected string $size = "";


    public function setAttribute($size){
        $this->size = $size;
    }

    public function getType(){
        return $this->type;
    }

    public function getAttribute(){
        if($this->size){
            return $this->size;
        }
    }
    public function getSize(){
        if($this->size){
            return $this->size;
        }
    }
    public function getLabel(){
        return "Size";
    }


    public function attributes(): array
    {
        return ["sku", 'name', 'price', 'type', "size"];
    }

    public function rules(): array
    {
        return [
            "sku" => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 10], [self::RULE_MAX, 'max' => 10], [self::RULE_UNIQUE, 'class' => self::class]],
            "name" => [self::RULE_REQUIRED],
            "price" => [self::RULE_REQUIRED, [self::RULE_NUMERIC]],
            "type" => [self::RULE_REQUIRED],
            "size" => [self::RULE_REQUIRED] . [self::RULE_NUMERIC],
        ];
    }

    public function populateClassAttributes(array $item)
    {
        $this->setSku($item['sku']);
        $this->setName($item['name']);
        $this->setPrice($item['price']);
        $this->setAttribute($item['size']);
    }

}
