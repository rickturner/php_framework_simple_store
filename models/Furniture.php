<?php

namespace app\models;
use app\models\Product;

class Furniture extends Product{
    
    protected string $type = "furniture";
    protected string $height = "";
    protected string $width = "";
    protected string $length = "";

    public function setAttribute($attributes){
        $this->height = $attributes[0];
        $this->width = $attributes[1];
        $this->length = $attributes[2];
    }

    public function getAttribute(){
        if ($this->height && $this->width && $this->length){
            return sprintf("%sX%sX%s", $this->height, $this->width, $this->length);

        }
    }

    public function getWidth(){
        if($this->width){
            return $this->width;
        }
    }
    public function getHeight(){
        if($this->height){
            return $this->height;
        }
    }
    public function getLength(){
        if($this->length){
            return $this->length;
        }
    }

    public function getType(){
        return $this->type;
    }

    public function getLabel(){
        return "Dimensions";
    }

    public function attributes(): array
    {
        return ["sku", 'name', 'price', 'type', "length", "width", "height"];
    }

    
    public function rules(): array
    {
        return [
            "sku" => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 10], [self::RULE_MAX, 'max' => 10], [self::RULE_UNIQUE, 'class' => self::class]],
            "name" => [self::RULE_REQUIRED],
            "price" => [self::RULE_REQUIRED, [self::RULE_NUMERIC]],
            "type" => [self::RULE_REQUIRED],
            "length" => [self::RULE_REQUIRED] . [self::RULE_NUMERIC],
            "width" => [self::RULE_REQUIRED] . [self::RULE_NUMERIC],
            "height" => [self::RULE_REQUIRED] . [self::RULE_NUMERIC],
        ];
    }

    public function populateClassAttributes(array $item)
    {
        $this->setSku($item['sku']);
        $this->setName($item['name']);
        $this->setPrice($item['price']);
        $this->setAttribute([$item['height'], $item['width'], $item['length']]);
    }
}