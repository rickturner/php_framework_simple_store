<?php

namespace app\models;

use app\models\Product;


class Book extends Product
{
    protected string $type = "book";
    protected string $weight = "";


    #SETTERS

    public function setAttribute($weight)
    {
        $this->weight = $weight;
    }

    #GETTERS

    public function getType()
    {
        return $this->type;
    }

    public function getAttribute()
    {
        if ($this->weight) {
            return $this->weight;
        }
    }

    public function getWeight()
    {
        if ($this->weight) {
            return $this->weight;
        }
    }

    public function getLabel()
    {
        return "Weight";
    }


    public function attributes(): array
    {
        return ["sku", 'name', 'price', 'type', "weight"];
    }

    public function rules(): array
    {
        return [
            "sku" => [self::RULE_REQUIRED, [self::RULE_MIN, 'min' => 10], [self::RULE_MAX, 'max' => 10], [self::RULE_UNIQUE, 'class' => self::class]],
            "name" => [self::RULE_REQUIRED],
            "price" => [self::RULE_REQUIRED, [self::RULE_NUMERIC]],
            "type" => [self::RULE_REQUIRED],
            "weight" => [self::RULE_REQUIRED] . [self::RULE_NUMERIC],
        ];
    }

    public function populateClassAttributes(array $item)
    {
        $this->setSku($item['sku']);
        $this->setName($item['name']);
        $this->setPrice($item['price']);
        $this->setAttribute($item['weight']);
    }
}
