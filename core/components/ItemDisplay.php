<?php

namespace app\core\components;

use app\core\Application;
use app\core\Request;
use PDO;

class ItemDisplay
{

    public function getItems()

    {
        $statement = Application::$app->db->pdo->prepare("SELECT * FROM items");
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getSkus()
    {
        $statement = Application::$app->db->pdo->prepare("SELECT sku FROM items");
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_COLUMN);
        return $result;
    }

    public function createCardItem($item)
    
    {
        // Product Initialization
        $className =  ucfirst($item['type']);
        $namespace = "\\app\\models\\";
        $className = "$namespace" . $className;
        $productModel = new $className;

        $productModel->populateClassAttributes($item);
        return $productModel->getHtmlContent();
        
    }


    

    static public function massDelete(array $requestBody){
        foreach($requestBody as $key=>$value){
            $statement = Application::$app->db->pdo->prepare("DELETE FROM items WHERE sku = '$key' ");
            // exit;
            $statement->execute();
        }
    }
}
